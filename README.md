### For in-depth details on how this project works, please refer to the [documentation](https://gitlab.com/kheina/kheina.com/wikis/home)  

as long as you can compile [iqdb](https://iqdb.org/code/) and run python3.6+ you should be able to run this project without a problem  

`sudo pip install -r requirements.txt`  

if you want to run the crawlers, the pycrawl lib is necessary:  
`pip install -e git+https://github.com/kheina/pycrawl.git#egg=pycrawl`  

Some frequently used terminal commands: (unfortunately I still use these)  
`truncate nohup.out --size=0`  
`sudo gunicorn -w 1 -k uvicorn.workers.UvicornWorker -b 0.0.0.0:80 server:api`  