from pycrawl.common import isint
from common import HTTPError
from PIL import ImageFile
import requests


def contains(list, filter) :
	for i in range(len(list)-1, -1, -1) :
		if filter(list[i]) : return i
	return None	


class ImageError(Exception) :
	pass


acceptedMimeTypes = {
	'image/jpeg',
	'image/png',
	'image/gif',
	'application/octet-stream',
}


def loadimagedata(url) :
	with requests.get(url, stream=True) as response :  # stream=True IS REQUIRED
		if response.ok :
			if 'Content-Type' in response.headers and response.headers['Content-Type'] not in acceptedMimeTypes :
					raise HTTPError.UnsupportedMedia('File is not of type jpg, png, or gif.')
			if 'Content-Length' in response.headers and isint(response.headers['Content-Length']) > 8388608 :  # 8192kB in bytes
					raise ImageError('Image size greater than 8192kB.')
			image = ImageFile.Parser()
			for chunk in response.iter_content(chunk_size=1024) :
				image.feed(chunk)
				if image.image :
					x, y = image.image.size
					pixels = x*y
					if pixels > 56250000 :  # 56250000 = 7500*7500
						raise ImageError('Image contains more than 56250000 pixels (contains ' + str(pixels) + ')')
					return image.data + response.content
			raise HTTPError.UnsupportedMedia('File is not of type jpg, png, or gif.')


def getimageinfofromurl(url) :
	try :
		with requests.get(url, stream=True) as response :  # stream=True IS REQUIRED
			if response.status_code == 200 :
				image = ImageFile.Parser()
				for chunk in response.iter_content(chunk_size=512) :
					image.feed(chunk)
					if image.image :
						x, y = image.image.size
						pixels = x*y
						return (x,y,pixels)
	except : return None
