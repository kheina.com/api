from pycrawl.common import GetFullyQualifiedClassName, isint
from common.HTTPError import UnsupportedMedia
from collections import defaultdict
from traceback import format_tb
from common import logging
import ujson as json
import subprocess
import colorama
import psycopg2
import socket
import time
import sys
import os


conn = None
logger = None


class IQDBerror(Exception) :
	pass


class DatabaserError(Exception) :
	pass


def isbinary(binary) :
	try : return binary.decode()
	except : return None


def isHex(hexString) :
	try : return int(hexString, 16)
	except : return None


invalidSha1s = {
	int('421d953965879c096e147cf7f443fe19e869cd55', 16),
	int('a543f80c56bfc21a593644caf4d76d7eacf43365', 16),
	int('83203dff7ec25dd6c11feb0182a073aaaf4befdb', 16),
	int('c5dcdc5995db87a9035834d40783b95b26537a97', 16),
	int('86c8ed3cc8e1ee5fb4c343f76d9cc2b6a7cce6a2', 16),
	int('e2e331e81ccfb8260fe1f1329183a44a0244619f', 16),
	int('2a2562f1cead4c30daaaf4d795ff7e968649473b', 16),
}
def validateSha1(sha1) :
	# returns the sha1 in int format or raises ValueError if invalid, or TypeError if it cannot be converted
	try :
		sha1 = int(sha1, 16)
	except :
		raise TypeError('value could not be converted to int.')

	if sha1 in invalidSha1s :
		raise ValueError('sha1 is invalid.')

	return sha1


def checkValueWithinMargin(value1, value2, margin) :
	return abs(value1 - value2) <= margin


def queryiqdb(imagedata) :
	message = f'query 0 0 25 :{len(imagedata)}\n'.encode() + imagedata
	host = 'localhost'
	port = 7000
	# timeout to prevent deadlocks
	with socket.create_connection((host, port), 5) as s :
		s.recv(1024)  # 000 iqdb ready (can be ignored)
		s.sendall(message)
		data = b''
		for i in range(10) :  # to prevent deadlocks
			data += s.recv(1024)  # keep receiving data until 000 iqdb ready
			if data.endswith(b'ready\n') : break

	if data :
		queryobject = { }
		data = data.splitlines()
		for response in data :
			if response.startswith(b'200') :
				status, iqdbid, similarity = response.split()[:3]
				queryobject[iqdbid.decode().rjust(12, '0')] = { 'similarity': float(similarity), 'sources': [] }
			elif response.startswith(b'301') : raise UnsupportedMedia('File is not an IQDB accepted type.')
			elif response.startswith(b'302') : raise IQDBerror(response)
		return querysimilarity(queryobject)
	else : raise IQDBerror(data)
	return 0, data


def query(sql, params=(), maxattempts=2) :
	if not conn or conn.closed :
		start(readonly=True)

	try :
		cur = conn.cursor()
		cur.execute(sql, params)
		return cur.fetchall()

	except psycopg2.DataError :
		e, exc_tb = sys.exc_info()[1:]
		logger.warning({ 'error': f'{GetFullyQualifiedClassName(e)}: {e}', 'stacktrace': format_tb(exc_tb) })
		# now attempt to recover by rolling back
		conn.rollback()

	except (psycopg2.IntegrityError, psycopg2.errors.InFailedSqlTransaction) :
		start(readonly=True, forceconnect=True)
		if maxattempts > 1 :
			e, exc_tb = sys.exc_info()[1:]
			logger.warning({ 'error': f'{GetFullyQualifiedClassName(e)}: {e}', 'stacktrace': format_tb(exc_tb) })
			return query(data, maxattempts=maxattempts-1)
		else :
			logger.exception({ })
			raise

	finally :
		# don't commit to avoid modifying or corrupting the database
		cur.close()


iqdbmap = ('sha1', 'title', 'source', 'artist', 'rating', 'websiteid')
def querysimilarity(data) :
	fetchall = query("""
	SELECT encode(images.iqdbid, 'hex'), encode(images.sha1, 'hex'), images.title, sources.url, artists.name, image_rating.ratingid, source_website.websiteid
	FROM images
		INNER JOIN image_source ON images.id = image_source.imageid
		INNER JOIN sources ON image_source.sourceid = sources.id
		INNER JOIN source_artist ON sources.id = source_artist.sourceid
		INNER JOIN artists ON source_artist.artistid = artists.id
		INNER JOIN image_rating ON images.id = image_rating.imageid
		INNER JOIN source_website ON sources.id = source_website.sourceid
	WHERE images.iqdbid IN ('\\x""" + "','\\x".join(data.keys()) + "')")
	for result in fetchall :
		data[result[0]]['sources'].append(dict(zip(iqdbmap, result[1:])))

	# discard the iqdbid keys since we don't need them anymore, and just return the (presorted) values
	return list(data.values())


artistmap = ('sha1', 'title', 'source', 'artist', 'rating', 'websiteid', 'timestamp')
def queryartist(artist, pagenum, maxResults) :
	if not conn or conn.closed :
		start(readonly=True)
	# doing some input validation
	if not isinstance(pagenum, int) or pagenum < 0 :
		raise ValueError('page must be an integer greater than or equal to zero.')
	if not isinstance(maxResults, int) or maxResults <= 0 :
		raise ValueError('count must be an integer greater than zero.')
	maxResults = min(maxResults, 100)
	if artist :
		fetchall = query("""
		SELECT encode(images.sha1, 'hex'), images.title, sources.url, artists.name, image_rating.ratingid, source_website.websiteid, extract(EPOCH FROM images.date)
		FROM artists
			INNER JOIN source_artist ON artists.id = source_artist.artistid
			INNER JOIN sources ON source_artist.sourceid = sources.id
			INNER JOIN image_source ON sources.id = image_source.sourceid
			INNER JOIN images ON image_source.imageid = images.id
			INNER JOIN image_rating ON images.id = image_rating.imageid
			INNER JOIN source_website ON sources.id = source_website.sourceid
		WHERE artists.namehash=md5(lower(%s))::uuid
		ORDER BY images.date DESC
		LIMIT %s OFFSET %s;""", (artist, maxResults, maxResults * pagenum))
		results = [dict(zip(artistmap, r)) for r in fetchall]

		return results


imagemap = ('sha1', 'title', 'timestamp', 'width', 'height', 'source', 'imageurl', 'tags', 'artist', 'artisturl', 'rating', 'websiteid')
def queryimage(sha1) :
	# make the strategic decision here to not use images.id so that we can't be crawled sequentially
	# convert the hex to an int to verify it's not malicious
	sha1 = validateSha1(sha1)

	fetchall = query("""
	SELECT
		encode(images.sha1, 'hex'), images.title, extract(EPOCH FROM images.date), images.width, images.height,
		sources.url, sources.imageurl, sources.tags,
		artists.name, artists.url,
		ratingid, source_website.websiteid
	FROM images
		INNER JOIN image_source ON images.id = image_source.imageid
		INNER JOIN sources ON image_source.sourceid = sources.id
		INNER JOIN source_artist ON sources.id = source_artist.sourceid
		INNER JOIN artists ON source_artist.artistid = artists.id
		INNER JOIN image_rating ON images.id = image_rating.imageid
		INNER JOIN source_website ON sources.id = source_website.sourceid
	""" f"WHERE images.sha1 = '\\x{sha1:040x}'")
	results = [dict(zip(imagemap, r)) for r in fetchall]

	return results


def querytags(whitelist, blacklist=None, minMatch=None, maxResults=24, page=0) :
	if not whitelist:
		raise ValueError('at least one tag must be passed.')
	if not isinstance(maxResults, int) or maxResults <= 0 :
		raise ValueError('count must be an integer greater than zero.')
	if not minMatch :
		minMatch = len(whitelist)
	elif not isinstance(minMatch, int) or minMatch <= 0 :
		raise ValueError('minMatch must be an integer greater than zero.')

	if blacklist :
		fetchall = query("""
		SELECT imageid, COUNT(*)
		FROM tags
		INNER JOIN tag_image ON
			imageid NOT IN (
				SELECT imageid
				FROM tags
				INNER JOIN tag_image ON COALESCE(aliasOf, id) = tagid
				WHERE tags.name IN %s
			)
			AND COALESCE(aliasOf, id) = tagid
		WHERE tags.name IN %s
		GROUP BY imageid
		HAVING COUNT(*) >= %s
		ORDER BY COUNT(*) DESC
		LIMIT %s OFFSET %s;
		""",
		(blacklist, whitelist, minMatch, maxResults, page))
	else :
		fetchall = query("""
		SELECT imageid, COUNT(*)
		FROM tags
		INNER JOIN tag_image ON COALESCE(aliasOf, id) = tagid
		WHERE tags.name IN %s
		GROUP BY imageid
		HAVING COUNT(*) >= %s
		ORDER BY COUNT(*) DESC
		LIMIT %s OFFSET %s;
		""",
		(whitelist, minMatch, maxResults, page))

	# SELECT encode(images.iqdbid, 'hex'), encode(images.sha1, 'hex'), images.title, sources.url, artists.name, image_rating.ratingid, source_website.websiteid
	# just use iqdb map
	return fetchall


requiredkeys = { 'id', 'tags', 'url', 'website', 'imageurl', 'timestamp', 'thumbnails', 'title', 'resolution', 'artisturl', 'filename', 'rating', 'sha1', 'artist' }
websitemap = defaultdict(lambda : 0, {  # assume furaffinity since it's the most populous
	'furaffinity': 0,
	'inkbunny': 1,
	'weasyl': 2,
	'furrynetwork': 3,
})
ratingmap = defaultdict(lambda : 2, {  # assume adult because it's the worst case
	'general': 0,
	'mature': 1,
	'adult': 2,
	'explicit': 2,
})
def updatedatabase(metadata, cli=None) :
	initialtime = time.time()
	log = {'origin': 'database update'}
	logtype = 'INFO'
	iqdbfile = []  # 'virtual' file
	iqdbsize = -1
	with open('iqdbsize.txt', 'r') as iqdbsizefile :
		iqdbsize = int(iqdbsizefile.read())
	print('iqdbsize:', iqdbsize)
	stats = { }
	print('loading stats.json...', end='', flush= True)
	try :
		with open('stats.json', 'r') as dbstats :
			stats = json.load(dbstats)
	except : print('could not load stats.json')
	else : print('success.')

	oldsources = stats['sources']
	oldartists = stats['artists']
	oldimages = stats['images']

	print('connecting to database kheina...', end='', flush= True)
	cur = conn.cursor()
	if conn.closed > 0 : raise DatabaserError('could not connect to database kheina')
	print('success. (' + str(conn.closed) + ')')

	log['furaffinityItems'] = 0
	log['furrynetworkItems'] = 0
	log['updatedItems'] = 0

	if 'fan' not in stats : stats['fan'] = -1
	if 'fao' not in stats : stats['fao'] = float('inf')
	if 'fnn' not in stats : stats['fnn'] = -1
	if 'fno' not in stats : stats['fno'] = float('inf')
	metalog = []
	for meta in metadata :
		metalog.append(meta)
		# check to make sure the update node is valid
		if not isinstance(meta, dict) or not requiredkeys.issubset(meta.keys()) :
			logger.error({ 'error': f'item is not able to be consumed as it does not contain required keys: {", ".join(requiredkeys - meta.keys())}.', 'item': meta })
			continue

		try :

			artist = meta['artist'].lower()
			cur.execute('SELECT artists.id FROM artists WHERE namehash=md5(%s)::uuid AND name=%s;', (artist, meta['artist']))
			artistid = cur.fetchall()

			if artistid :
				# if the artist already exists, grab the id
				artistid = artistid[0][0]
			else :
				cur.execute("""
				INSERT INTO artists (name, namehash, url) VALUES (%s, md5(%s)::uuid, %s);
				SELECT currval('artists_id_seq');""",
				(meta['artist'], artist, meta['artisturl']))
				artistid = cur.fetchall()[0][0]
				stats['artists'] += 1

			cur.execute("SELECT id FROM sources WHERE urlhash=md5(%s)::uuid AND url=%s;", (meta['url'], meta['url']))
			sourceid = cur.fetchall()
			cur.execute("SELECT id FROM images WHERE sha1=%s;", (f"\\x{meta['sha1']}",))
			imageid = cur.fetchall()

			ratingid = ratingmap[meta['rating'].lower()]

			if sourceid :
				# the source already exists, and may be updated
				sourceid = isint(sourceid[0][0])
				imageid = isint(imageid[0][0])  # fix this issue too

				cur.execute("""
				UPDATE images
					SET title = %s
					WHERE images.id = %s;
				UPDATE sources
					SET tags = %s
					WHERE sources.id = %s;
				UPDATE image_rating
					SET ratingid = %s
					WHERE imageid = %s and ratingid < %s;""",
				(meta['title'], imageid,
				meta['tags'], sourceid,
				ratingid, imageid, ratingid))

				log['updatedItems'] += 1

			else :
				# the source does not exist, and must be added
				websiteid = websitemap[meta['website'].lower()]

				if imageid :
					imageid = isint(imageid[0][0])

					cur.execute("""
					INSERT INTO sources (url, urlhash, imageurl, tags) VALUES (%s, md5(%s)::uuid, %s, %s);
					INSERT INTO image_source (imageid, sourceid) VALUES (%s, currval('sources_id_seq'));
					INSERT INTO source_website (sourceid, websiteid) VALUES (currval('sources_id_seq'), %s);
					INSERT INTO source_artist (sourceid, artistid) VALUES (currval('sources_id_seq'), %s);
					UPDATE image_rating SET ratingid = %s WHERE imageid = %s and ratingid < %s;""",
					(meta['url'], meta['url'], meta['imageurl'], meta['tags'],
					imageid,
					websiteid,
					artistid,
					ratingid, imageid, ratingid))

					stats['sources'] += 1
				else :
					x = y = resolution = meta.get('resolution')
					if resolution :
						x, y = resolution

					iqdbid = f'{iqdbsize:012x}'

					cur.execute("""
					INSERT INTO images (iqdbid, sha1, thumbnails, title, date, width, height) VALUES (%s, %s, %s, %s, to_timestamp(%s), %s, %s);
					INSERT INTO sources (url, urlhash, imageurl, tags) VALUES (%s, md5(%s)::uuid, %s, %s);
					INSERT INTO image_rating (imageid, ratingid) VALUES (currval('images_id_seq'), %s);
					INSERT INTO source_website (sourceid, websiteid) VALUES (currval('sources_id_seq'), %s);
					INSERT INTO image_source (imageid, sourceid) VALUES (currval('images_id_seq'), currval('sources_id_seq'));
					INSERT INTO source_artist (sourceid, artistid) VALUES (currval('sources_id_seq'), %s);""",
					(f"\\x{iqdbid}", f"\\x{meta['sha1']}", meta['thumbnails'], meta['title'], meta['timestamp'], x, y,
					meta['url'], meta['url'], meta['imageurl'], meta['tags'],
					ratingid,
					websiteid,
					artistid))

					if x and y :
						iqdbfile.append(f"{iqdbid} {x} {y}:{meta['filename']}")
					else :
						iqdbfile.append(f"{iqdbid}:{meta['filename']}")

					if websiteid == 0 :  # furaffinity
						if meta['id'] > stats['fan'] : stats['fan'] = meta['id']
						elif meta['id'] < stats['fao'] : stats['fao'] = meta['id']
						log['furaffinityItems'] += 1
					elif websiteid == 3 :  # furrynetwork
						if meta['id'] > stats['fnn'] : stats['fnn'] = meta['id']
						elif meta['id'] < stats['fno'] : stats['fno'] = meta['id']
						log['furrynetworkItems'] += 1

					stats['images'] += 1
					stats['sources'] += 1
					iqdbsize += 1

		except :
			logger.exception({
				'info': 'the item encountered an error while being consumed. all or part of the item may have been stored in the database.',
				'item': meta,
			})

	if cli : print('\rdone.')

	iqdbfile = '\n'.join(iqdbfile).encode()
	if not updateiqdb(iqdbfile) :  # if iqdb fails, save a copy
		with open(f'iqdb_{iqdbsize}.txt', 'wb') as iqdbdump :
			iqdbdump.write(iqdbfile)
		logtype = 'CRITICAL'
		log['error'] = 'an error occurred during iqdb update. The data has been transferred to iqdb_' + str(iqdbsize) + '.txt.\nPlease fix the error and consume the file using ' + colorama.Fore.GREEN + 'databaser.py iqdb iqdb_' + str(iqdbsize) + '.txt' + colorama.Style.RESET_ALL
		print(log['error'])

	else :  # if iqdb passes then clear image folder
		for meta in metalog :
			filename = meta['filename']
			try :
				if os.path.isfile(filename) :  # but only images that were consumed this update
					os.remove(filename)
			except :
				logger.exception({ 'info': 'an exception occurred during iqdb update.' })

	conn.commit()
	cur.close()

	with open('iqdbsize.txt', 'w') as iqdbsizefile :
		iqdbsizefile.write(str(iqdbsize))

	newsources = stats['sources'] - oldsources
	log['crawledItems'] = len(metalog)
	log['newImages'] = stats['images'] - oldimages
	log['newArtists'] = stats['artists'] - oldartists
	log['newSources'] = newsources
	log['totalItems'] = log['furaffinityItems'] + log['furrynetworkItems']

	stats['lastupdate'] = time.time()
	with open('stats.json', 'w') as dbstats :
		json.dump(stats, dbstats)

	result = { 'elapsedtime': time.time() - initialtime, 'stats': stats, 'newsources': newsources }
	log.update(result)

	if logtype != 'INFO' :
		logger.error(log)
	else :
		logger.info(log)

	return result

iqdberrors = (b'what()', b'terminate', b'throwing', b'error')
def updateiqdb(iqdbfile) :
	if iqdbfile :
		p = subprocess.Popen(('./iqdb/iqdb', 'add', 'main.db'), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		output = p.communicate(input=iqdbfile)
		output = b'\n'.join(output)
		if any(string in output for string in iqdberrors) :
			print(output)
			return False
	return True

def checkpostgres() :
	print('connecting to database kheina...', end='', flush= True)
	cur = conn.cursor()
	if conn.closed > 0 : return 'could not connect to database kheina'
	print('success. (' + str(conn.closed) + ')')

	cur.execute("SELECT COUNT(iqdbid) FROM images;")
	print(f' images: {cur.fetchall()[0][0]:10,}')
	cur.execute("SELECT COUNT(id) FROM sources;")
	print(f'sources: {cur.fetchall()[0][0]:10,}')
	cur.execute("SELECT COUNT(id) FROM artists;")
	print(f'artists: {cur.fetchall()[0][0]:10,}')
	cur.close()

	return 'woot'

def rebuildstats() :
	stats = { 'images': 0, 'sources': 0, 'artists': 0 }

	cur = conn.cursor()
	for key in stats.keys() :
		cur.execute(f'SELECT COUNT(id) FROM {key};')
		stats[key] = cur.fetchall()[0][0]
		print(f'{key.rjust(15)}: {stats[key]:11,}')
	cur.close()

	stats.update({ 'fan': -1, 'fao': 9999999999, 'fnn': -1, 'fno': 9999999999, 'lastupdate': time.time() })

	with open('stats.json', 'w') as st :
		json.dump(stats, st)

def psql() :
	query = input('> ')
	while query and query != '\q' :
		initialtime = time.time()
		try :
			if query == 'commit' :
				conn.commit()
				print('changes committed.')
			else :
				with run(query) as cur :
					print(cur.fetchall())
		except Exception as e :
			print(e)
		print(time.time() - initialtime)
		query = input('> ')

def run(query, params=[]) :
	cur = conn.cursor()
	cur.execute(query, params)
	return cur

def deldb() :
	input('WARNING! THIS WILL DELETE YOUR ENTIRE DATABASE (control-C to exit)')
	try :
		cur = conn.cursor()

		cur.execute("DROP TABLE image_rating;")
		cur.execute("DROP TABLE image_source;")
		cur.execute("DROP TABLE image_artist;")
		cur.execute("DROP TABLE source_website;")
		cur.execute("DROP TABLE images;")
		cur.execute("DROP TABLE sources;")
		cur.execute("DROP TABLE artists;")
		cur.execute("DROP TABLE ratings;")
		cur.execute("DROP TABLE websites;")

		os.remove('databaseIDs.json')
		os.remove('main.db')
		os.remove('stats.json')
		os.remove('iqdbsize.txt')

		conn.commit()
		cur.close()
		print('done.')
	except : print('there was an error, the database was not cleared. you may not have one of the following files: databaseIDs.json, main.db, stats.json, iqdbsize.txt')

def start(log=None, readonly=True, forceconnect=None) :
	global logger
	global conn

	logger = log if log else logging.getLogger('databaser')

	try :
		with open('credentials/postgres.json', 'r') as credentials :
			credentials = json.load(credentials)
			if not conn or conn.closed or forceconnect :
				if conn :
					conn.close()
				conn = psycopg2.connect(dbname='kheina', user=credentials['user'], password=credentials['password'], host=credentials['host'], port='5432')
				if readonly :
					conn.set_session(readonly=True)
			else :
				print('database already connected')
	except Exception as e :
		print(f'{colorama.Fore.RED}WARNING{colorama.Style.RESET_ALL}: postgres connection failed! Searches are impossible!\n    >{e}')


def cleanup(cli=False) :
	global conn
	conn.close()
	if cli :
		if conn.closed :
			print('database connection closed successfully.')
		else :
			print(f"{colorama.Fore.RED}WARNING{colorama.Style.RESET_ALL}: database connection didn't close!")

def help() :
	print('help  ------ brings up this dialogue')
	print('init  ------ initializes postgres database and creates other misc files')
	# print('update  ---- runs a manual update on the files listed after the keyword (or FAmeta.txt, FNmeta.txt, FAmetaold.txt, FNmetaold.txt by default)')
	print('iqdb  ------ runs a manual iqdb update on the files listed after the keyword')
	print('check  ----- SELECTs a range of data from all records from the database to ensure functionality')
	print('drop  ------ deletes the database and other misc files')
	print('thumbnails - manually redownloads thumbnails from the files listed after the keyword')
	print()


if __name__ == '__main__' :
	try :
		start()
		query = False
		method = 'help'
		params = []
		if len(sys.argv) > 1 :
			method = sys.argv[1]
			if len(sys.argv) > 2 :
				for i in range(2, len(sys.argv)) :
					params.append(sys.argv[i])
		if method == 'init' :
			initializepostgres()
		elif method == 'update' :
			if params : print(updatedatabase(params, cli=True))
			else : print(updatedatabase(['FAmeta.txt', 'FNmeta.txt', 'FAmetaold.txt', 'FNmetaold.txt'], cli=True))
		elif method == 'iqdb' :
			with open(params[0], 'rb') as iqdbfile :
				contents = iqdbfile.read()
			updateiqdb(contents)
		elif method == 'upload' :
			if params : print(uploadimagesfromfolder(params[0]))
			else : print(uploadimagesfromfolder('images'))
		elif method == 'check' :
			checkpostgres()
		elif method == 'drop' :
			deldb()
		elif method == 'thumbnails' :
			if params : redownloadthumbnails(params)
			else : redownloadthumbnails(['FAmeta.txt', 'FNmeta.txt', 'FAmetaold.txt', 'FNmetaold.txt'])
		elif method == 'psql' :
			psql()
		else : help()
	finally :
		cleanup(cli=True)
