from FAcrawler import FurAffinityCrawler

class FurAffinityCrawlValidator(FurAffinityCrawler) :

	def webcodeGenerator(self) :
		import databaser
		while not self.done() :
			databaser.start(readonly=True)
			cur = databaser.run("""
				SELECT sources.id, split_part(sources.url, '/', 5) AS webcode
				FROM sources
					INNER JOIN source_website
						ON sources.id = sourceid
						AND websiteid = 0
				WHERE sources.id < %s
				ORDER BY sources.id DESC
				LIMIT 100;""",
				(self.id,)
			)

			results = cur.fetchall()
			databaser.cleanup()

			for r in results :
				self.id = r[0]
				yield int(r[1])
				if self.done() : break

	def urlGenerator(self) :
		# create generator for url webcodes
		genny = self.webcodeGenerator()
		prev = next(genny)
		for curr in genny :
			if abs(prev - curr) > 1 :
				for webcode in range(min(prev, curr) + 1, max(prev, curr)) :
					yield webcode
			prev = curr
