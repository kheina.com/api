import subprocess

def getIds() :
	call = './iqdb/iqdb list main.db'.split()

	p = subprocess.Popen(call, stdout=subprocess.PIPE)

	output = '\n'.join(c.decode() for c in p.communicate() if c)

	output = set(int(a, 16) for a in output.split())

	missing = set(range(min(output), max(output))) - output

	return tuple(f'{m:012x}' for m in missing)

if __name__ == '__main__' :
	print("SELECT encode(iqdbid, 'hex'), encode(sha1, 'hex') FROM images WHERE iqdbid IN \n('" + "','".join(f'\\x{m}' for m in getIds()) + "');")
