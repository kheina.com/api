from crawler import Crawler, ResponseNotOk, ShutdownCrawler, first
from parsesource import isint
import itertools
import requests
import time

sign = lambda a: (a>0) - (a<0)

class BackBlazeThumbnailCrawler(Crawler) :
	def __init__(self, *args, batchsize=50, **kwargs) :
		self.batchsize = batchsize
		super().__init__(*args, **kwargs)

	def run(self) :
		if self.id is None :
			raise ShutdownCrawler('startingid must be an integer')

		if not isint(self.direction) :
			raise ShutdownCrawler('direction must be an integer')

		nextcheck = time.time() + self.checkevery

		try :
			while not self.event.is_set() :
				with open('iqdb_data.txt') as data :
					lines = tuple(tuple(l.split()) for l in itertools.islice(data, self.id, self.id + self.batchsize))

				for i in range(0, self.batchsize, self.direction) :
					if self.crawl(lines[i]) :
						self.skips = 0
					else :
						self.skipped[0].append(lines[i])
						self.skips += 1

				self.id += self.batchsize * sign(self.direction)

				self.logger.agent.log_text(f'{self.name} finished up to id {self.id}', severity='INFO')

				if time.time() > nextcheck :
					self.checkSkips()
					nextcheck = time.time() + self.checkevery

		except ShutdownCrawler :
			pass

		self.logger.agent.log_text(f'{self.name} gracefully shutting down. current id: {self.id} ({self.totalSkipped()}) {self.checkevery}s', severity='INFO')

		# try to gracefully shut down...
		maxChecks = len(self.skipped)
		while self.totalSkipped() and maxChecks > 0 :
			time.sleep(self.checkevery)
			self.checkSkips()
			maxChecks -= 1

		self.logger.agent.log_text(f'{self.name} gracefully finished. current id: {self.id}, {self.totalSkipped()} skipped items left: {self.skipped}', severity='ERROR' if self.totalSkipped() else 'INFO')

	def crawl(self, url) :
		try :
			filename, sha1 = url
			success = downloadimageforiqdb(sha1, filename)
			return success

		except Exception as e :
			typeE = type(e)
			if typeE in self.errorHanders :
				self.errorHanders[typeE](self.id)
			else :
				self.shutdown(e)

		return False

def downloadimageforiqdb(sha1, filename) :
	url = f'https://cdn.kheina.com/file/kheinacom/{sha1}.jpg'
	with requests.get(url, stream=True) as response :  # stream=True IS REQUIRED
		if response.ok :
			imagedata = response.content
			filename = f'images/{filename}.jpg'
			with open(filename, 'wb') as image :  # can do this on another process
				image.write(imagedata)
			return True
		else :
			raise ResponseNotOk(f'response not ok: {response.reason}', status=response.status_code)
	raise ShutdownCrawler('failed to download image for iqdb.')
