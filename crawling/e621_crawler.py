from pycrawl.crawler import ResponseNotOk, InvalidResponseType, NoSubmission
from crawling.baseCrawler import BaseCrawler
from datetime import datetime
from jmespath import search


class MissingData(Exception) :
	pass


class E621Crawler(BaseCrawler) :

	rating_map = {
		's': 'general',
		'q': 'mature',
		'e': 'explicit',
	}

	def raiseNoSubmission(self, *args, **kwargs) :
		raise NoSubmission('no submission here')


	def __init__(self, *args, **kwargs) :
		BaseCrawler.__init__(self, *args, route='no-store.new', key='e621', domain='e621.net', **kwargs)
		self.errorHandlers[MissingData] = self.skipUrl
		self.unblocking[404] = self.raiseNoSubmission


	def formatUrl(self, url) :
		return f'https://e621.net/posts/{url}.json'


	def documentMaker(self, response) :
		print(response.text)
		return response.json()


	def parse(self, document) :
		if not search('post.sources', document) :
			raise MissingData('source')

		artist = ', '.join(search('post.tags.artist', document))

		if not artist :
			raise MissingData('artist')

		return {
			'url': self.formattedurl[:-5],
			'imageurl': search('post.sample.url', document),
			'id': self.url,
			'resolution': search('post.file.[width, height]', document),
			'artist': artist,
			'artisturl': None,
			'title': None,
			'rating': E621Crawler.rating_map[search('post.rating', document)],
			'tags': search('post.tags.* | []', document) + ['e621'],
			'timestamp': datetime.strptime(search('post.created_at', document).replace(':', ''), '%Y-%m-%dT%H%M%S.%f%z').timestamp(),
			'website': 'e621',
		}
