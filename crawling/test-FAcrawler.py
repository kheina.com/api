from FAcrawler import FurAffinityCrawler
from multiprocessing import Event
from lxml.html import fromstring
from traceback import format_tb
try : import ujson as json
except : import json
import sys


testfiles = {
	'fa-test.html': { 'url': 'https://www.furaffinity.net/view/33345736', 'imageurl': 'https://d.facdn.net/art/loimu/1570562412/1570562412.loimu_n5.png', 'id': 33345736, 'resolution': (1280, 833), 'artist': 'Loimu', 'artisturl': 'https://www.furaffinity.net/user/loimu/', 'title': 'Marja', 'rating': 'Adult', 'tags': ['loimu', 'minttumania', 'Marja', 'dog', 'shiba', 'inu', 'canine', 'k9', 'feral', 'furry', 'collar', 'pet', 'blowjob', 'felatio', 'oral', 'cum', 'nsfw', 'art', 'dogo'], 'timestamp': 1570562412, 'thumbnails': ['http://t.facdn.net/33345736@200-1570562412.jpg', 'http://t.facdn.net/33345736@300-1570562412.jpg', 'http://t.facdn.net/33345736@400-1570562412.jpg'], 'website': 'Fur Affinity', 'sleepfor': 30 },
	'fa-test-2.html': { 'url': 'https://www.furaffinity.net/view/34216971', 'id': 34216971 },
	'fa-test-3.html': { 'url': 'https://www.furaffinity.net/view/2839453', 'id': 2839453 },
	'fa-test-4.html': { 'url': 'http://www.furaffinity.net/view/33345736', 'id': 33345736 },
	'fa-test-5.html': { 'url': 'http://www.furaffinity.net/view/33345736', 'id': 33345736 },
}

for filename, result in testfiles.items() :
	print('test:', filename)
	try :
		with open(f'test/{filename}') as test :
			html = fromstring(test.read())
		results = FurAffinityCrawler.parse(None, html, result['url'], result['id'])
		if results != result :
			print(*(f'{result.get(k)} != {v}' for k, v in results.items() if result.get(k) != v))
		_ = json.dumps(results).encode().decode()
		print(_)
	except Exception as e :
		exc_type, exc_obj, exc_tb = sys.exc_info()
		stacktrace = format_tb(exc_tb)
		exc_obj = str(e)
		error = f'{e.__class__.__name__}: {exc_obj}\n	stacktrace:\n'
		for framesummary in stacktrace :
			error = error + framesummary
		print(error)
	print()
