from pycrawl.crawler import Crawler, BaseCrawlingException, ShutdownCrawler, InvalidResponseType
from pycrawl.common import GetFullyQualifiedClassName
from pycrawl.common.HTTPError import ResponseNotOk
from common.databaser import validateSha1
from common.crawling import normalizeTag
from hashlib import sha1 as hashlib_sha1
from common.HTTPError import HTTPError
from collections import defaultdict
from importlib import import_module
from lxml.html import fromstring
from traceback import format_tb
from kh_common import logging
from base64 import b64encode
from PIL import ImageFile
import ujson as json
import requests
import time
import sys


class ImageDownloadError(BaseCrawlingException, HTTPError) :
	def __init__(self, message, logdata={ }, status=500) :
		BaseCrawlingException.__init__(self, message, logdata)
		HTTPError.__init__(self, message, status)


mimeTypeMap = {
	# type: (extension, mimetype)
	'JPEG': ('jpg', 'image/jpeg'),
	'GIF': ('gif', 'image/gif'),
	'PNG': ('png', 'image/png'),
	'WEBP': ('webp', 'image/webp'),
}


ratingmap = {
	'general': 'general',
	'mature': 'mature',
	'adult': 'explicit',
	'explicit': 'explicit',
}


class BaseCrawler(Crawler) :

	def __init__(self, *args, key=None, domain=None, **kwargs) :
		"""
		imageMaxRetries: how many times to try and download an image before adding to skips
		b2cutoff: how many images are allowed in the B2 queue before crawler sleeps or shuts down
		imagetimeout: how long to wait when downloading an image
		b2maxretries: number of times to retry uploading an image to B2 before giving up (images will enter a queue to retry)
		route: routing key to use for message queue publishing
		domain: domain to use for session (headers and cookies) storage
		"""
		super().__init__(*args, **kwargs)
		self.errorHandlers.update({
			# we know there's a submission here, it was crawled, but the image can't be downloaded for some reason
			ImageDownloadError: lambda : self.imageDownloadSkips.append(self.url),
		})
		self.doNotLog.add(ImageDownloadError)
		self.B2MaxRetries = int(kwargs.get('b2maxretries', 5))
		self.imageTimeout = float(kwargs.get('imagetimeout', 60))
		self.B2cutoff = int(kwargs.get('b2cutoff', 25))
		self.imageMaxRetries = int(kwargs.get('imagemaxretries', 2))
		self.B2skips = []
		self.imageDownloadSkips = []
		self.unblocking.update({
			# 503: self.unblockCF
		})
		self.logger = logging.getLogger(self.name.replace('+', '.'))
		self.thumbnailIndex = -1

		# redefine skipped as a list because google logging shits itself when you log a tuple
		self.skipped = list(self.skipped)

		# initialize B2
		self.B2 = None

		try :
			print('loading credentials...', end='', flush=True)
			with open('credentials/crawlers.json', 'r') as credentials :
				credentials = json.load(credentials)

				if key :
					# prime the session with headers and cookies
					website_credentials = credentials.get(key)
					if website_credentials :
						self._session.headers.update(website_credentials.get('headers', { }))

						# manually load cookies into the session
						for name, value in website_credentials.get('cookies', { }).items() :
							self._session.cookies.set(name=name, value=value, domain=domain)

				# authorize with B2
				if not self._authorizeB2(credentials['B2']) :
					raise ShutdownCrawler(f'B2 authorization handshake failed on startup.')

				# message queue setup
				if 'message_queue' not in credentials :
					raise ShutdownCrawler(f'message queue initialization failed on startup.')

				credentials = credentials["message_queue"]

				# update the default routing key with the passed one, should it exist
				if 'route' in kwargs :
					credentials['publish_info']['routing_key'] = kwargs['route']

				# establish initial connection to message queue
				self.mqConnect(credentials['connection_info'], credentials['exchange_info'], credentials['publish_info'])
			print('success.')

		except Exception as e :
			exc_type, exc_obj, exc_tb = sys.exc_info()
			self.logger.error({
				'error': f'{GetFullyQualifiedClassName(e)}: {e}',
				'stacktrace': format_tb(exc_tb),
				'id': self.id,
				'name': self.name,
				**getattr(e, 'logdata', { }),
			})
			raise ShutdownCrawler(e).with_traceback(exc_tb)

		self._request_meta = defaultdict(lambda : 0)
		self._request_count = 0
		self._request_timer = -1
		self._request_time_limit = 60

		self.logger.info(f'{self.name} initialized.')


	def _authorizeB2(self, B2credentials=None) :
		try :
			if not B2credentials :
				print('loading credentials...', end='', flush=True)
				with open('credentials/crawlers.json', 'r') as credentials :
					credentials = json.load(credentials)
					B2credentials = credentials['B2']
				print('success.')

			basic_auth_string = b'Basic ' + b64encode((B2credentials['keyId'] + ':' + B2credentials['key']).encode())
			B2headers = { 'Authorization': basic_auth_string }
			response = requests.get('https://api.backblazeb2.com/b2api/v2/b2_authorize_account', headers=B2headers, timeout=self.timeout)

		except :
			self.logger.error(self.crashInfo())

		else :
			if response.ok :
				self.B2 = json.loads(response.content)
				return True

			else :
				self.logger.error(f'B2 authorization handshake failed: {response.content}')

		return False


	def crashInfo(self) :
		ci = Crawler.crashInfo(self)
		return {
			**ci,
			'B2skips': len(self.B2skips),
			'imageskips': self.imageDownloadSkips,
		}


	def downloadHtml(self, url) :
		try :
			starttime = time.time()
			response = self._session.get(url, timeout=self.timeout)
			self._request_meta['request_time'] += time.time() - starttime
			self._request_count += 1
			if self._request_timer < time.time() :
				self._request_meta['request_time'] /= self._request_count
				self.logger.info(self._request_meta)
				self._request_meta.clear()
				self._request_count = 0
				self._request_timer = time.time() + self._request_time_limit
		except :
			raise ResponseNotOk(f'timeout ({self.timeout}) elapsed for url: {url}', status=-1)
		else :
			if response.ok :
				return self.documentMaker(response)
			elif response.status_code in self.unblocking :
				self.unblocking[response.status_code](response)
			raise ResponseNotOk(f'reason: {response.reason}, url: {url}', status=response.status_code, logdata={ 'reason': response.reason })
		raise InvalidResponseType(f'request failed for an unknown reason.')


	def documentMaker(self, response) :
		return fromstring(response.text)


	def skips(self) :
		return self.totalSkipped() + len(self.B2skips) + len(self.imageDownloadSkips)


	def checkSkips(self) :
		self.checkingSkips = True

		maxlen = len(self.skipped) - 1
		for i in range(maxlen, -1, -1) :
			while self.skipped[i] :
				url = self.skipped[i].pop()
				if self.crawl(url) :
					pass  # use pass rather than not because it's easier to read
				elif i < maxlen :
					self.skipped[i+1].append(url)

		B2errors = len(self.B2skips)
		for _ in range(B2errors) :
			self.uploadToB2(*self.B2skips.pop(0))

		imageDownloadErrors = len(self.imageDownloadSkips)
		for _ in range(imageDownloadErrors) :
			self.crawl(self.imageDownloadSkips.pop(0))

		self.checkingSkips = False

		self.logger.info({
			'name': self.name,
			'remainingB2errors': len(self.B2skips),
			'B2errors': B2errors,
			'imageDownloadErrors': imageDownloadErrors,
			'remainingImageDownloadErrors': len(self.imageDownloadSkips),
		})


	def verboseSkipped(self) :
		return f'{self.skipped}, image skips: {self.imageDownloadSkips}, B2 skips left: {len(self.B2skips)}'


	def postProcessing(self, item) :
		if 'thumbnails' in item :
			sha1, filename, extension = self.downloadimagefordatabase(item['thumbnails'][self.thumbnailIndex])
		
		else :
			sha1 = hashlib_sha1('|'.join([str(item['timestamp']), str(item['title']), item['artist'], item['website']]).encode()).hexdigest()
			filename = None
			extension = None

		rating = item['rating'].lower()

		item.update({
			'sha1': sha1,
			'filename': filename,
			'extension': extension,
			'tags': list(set(filter(None, map(normalizeTag, item['tags'])))),
			'rating': ratingmap[rating] if rating in ratingmap else rating,
		})


	def downloadimagefordatabase(self, imageurl) :
		response = None
		for _ in range(self.imageMaxRetries) :
			with requests.get(imageurl, stream=True, timeout=self.imageTimeout) as response :  # stream=True IS REQUIRED
				try :
					if response.ok :
						imagedata = response.content
						sha1 = hashlib_sha1(imagedata).hexdigest()
						validateSha1(sha1)
						parser = ImageFile.Parser()
						chunksize = 100
						for chunk in range(0, len(imagedata), chunksize) :
							parser.feed(imagedata[chunk:chunk+chunksize])
							if parser.image : break
						extension, mime = mimeTypeMap[parser.image.format]
						del parser
						filename = f'images/{sha1}.{extension}'
						with open(filename, 'wb') as image :  # can do this on another process
							image.write(imagedata)
						B2return = self.uploadToB2(imagedata, sha1, extension, mime)
						return sha1, filename, extension

				except Exception as e :
					if 'sha1' in str(e) :
						raise InvalidResponseType('website returned invalid image.', logdata={ 'sha1': sha1, **self.crashInfo() })
					# else pass, just retry

		status = response.status_code if response else -1
		raise ImageDownloadError(f'failed to download image for database. imageurl: {imageurl}', status=status)


	def uploadToB2(self, filedata, sha1, extension, contenttype) :
		# obtain upload url
		response = None
		upload = False
		for _ in range(self.B2MaxRetries) :
			try :
				response = requests.post(
					self.B2['apiUrl'] + '/b2api/v2/b2_get_upload_url',
					data='{ "bucketId": "' + self.B2['allowed']['bucketId'] + '" }',
					headers={ 'Authorization': self.B2['authorizationToken'] },
					timeout=self.timeout
				)
			except : pass
			else :
				if response.ok :
					upload = True
					break

				elif response.status_code == 401 :
					# obtain new auth token
					if not self._authorizeB2() :
						time.sleep(self.idleTime)

		if upload :
			# response ok
			response = json.loads(response.content)
			headers = { 'Authorization': response['authorizationToken'], 'X-Bz-File-Name': f'{sha1}.{extension}', 'Content-Type': contenttype, 'Content-Length': str(len(filedata)), 'X-Bz-Content-Sha1': sha1 }
			request = response['uploadUrl']
			for _ in range(self.B2MaxRetries) :
				try : response = requests.post(request, headers=headers, data=filedata, timeout=self.imageTimeout)
				except : pass
				else :
					if response.ok : return json.loads(response.content)

			if isinstance(response, requests.Response) :
				details = { 'error': 'image upload to B2 failed.', 'response': json.loads(response.content), 'status': response.status_code }
			else :
				details = { 'error': 'image upload to B2 failed.', 'response': response, 'status': None }
		elif response :
			# response not ok
			self.logger.error({
				'error': 'B2 image upload url handshake failed.',
				'name': self.name,
				'id': self.id,
				'response': json.loads(response.content),
				'status': response.status_code
			})
			raise ShutdownCrawler('B2 image upload url handshake failed.')
		else :
			# no response
			details = { 'error': 'B2 image upload url handshake failed.', 'response': str(response) }

		self.logger.info({
			'name': self.name,
			'id': self.id,
			**details,
		})

		# add to B2 skips as a tuple to expand again later
		self.B2skips.append((filedata, sha1, extension, contenttype))

		if len(self.B2skips) > self.B2cutoff :
			raise ShutdownCrawler(f'B2skips exceeded max length allowed ({self.B2cutoff}).')

		# sleep longer based on how many skipped images there are in memory currently
		time.sleep((len(self.B2skips) - 1)**2)


	def unblockCF(self, response) :
		GetCFCookies = getattr(import_module('cf.UnblockCF'), 'GetCFCookies')
		cookies = GetCFCookies(response)

		if 'cf_clearance' in cookies :
			self._session.cookies.update(cookies)
			self.logger.info({
				'name': self.name,
				'message': 'obtained new credentials.',
				'cookies': dict(cookies),
			})
		else :
			self.logger.error({
				'name': self.name,
				'message': 'failed to obtain new credentials.',
				'cookies': dict(cookies),
			})
