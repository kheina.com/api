from pycrawl.crawler import WebsiteOffline, InvalidResponseType, InvalidSubmission, NoSubmission, first
from pycrawl.common.HTTPError import BadOrMalformedResponse
from crawling.baseCrawler import BaseCrawler
from urllib.parse import urlparse
from pycrawl.common import isint
from datetime import datetime
import sys

class FurAffinityJournalCrawler(BaseCrawler) :

	submissionDateFormat = '%b %d, %Y %I:%M %p'


	def __init__(self, *args, **kwargs) :
		BaseCrawler.__init__(self, *args, key='furaffinity', domain='.furaffinity.net', **kwargs)


	def formatUrl(self, url) :
		return f'https://www.furaffinity.net/journal/{url}'


	def parse(self, document) :
		# check that the website isn't down and etc etc
		if first(document.xpath('//body//div[@class="attribution"]/a/text()', **self.xpathargs)) == 'DDoS protection by Cloudflare' :
			raise WebsiteOffline('furaffinity is currently behind cloudflare.')

		if first(document.xpath('//body/@id', **self.xpathargs)) == 'pageid-matureimage-error' :
			raise BadOrMalformedResponse('furaffinity login error.')

		elif document.xpath('//head/title[contains(text(), "System Error")]', **self.xpathargs) and document.xpath('//body/section/div[@class="section-body" and contains(text(), "The journal you are trying to find is not in our database")]', **self.xpathargs) :
			raise NoSubmission('url does not have a submission.')

		elif document.xpath('//body//section/div[contains(@class, "section-body")]/h2[contains(text(), "System Message")]', **self.xpathargs) :
			raise NoSubmission('url does not have a submission.')

		elif document.xpath('//img[@src="/fa_offline.jpg"]', **self.xpathargs) :
			raise WebsiteOffline('furaffinity is currently offline.')

		# now we can actually crawl

		upload = first(document.xpath('//div[@class="content"]//div[@class="section-header"]//span[@class="popup_date"]/text()', **self.xpathargs))
		if upload :
			upload = datetime.strptime(upload, FurAffinityJournalCrawler.submissionDateFormat)
		else :
			raise BadOrMalformedResponse('could not find upload date in html.')

		artisturl = first(document.xpath('//div[@id="user-profile"]//a[@class="current" and contains(@href, "/user/")]/@href', **self.xpathargs))
		if artisturl :
			artisturl = 'https://www.furaffinity.net' + artisturl
		else :
			raise BadOrMalformedResponse('could not find artist url in html.')

		artist = first(document.xpath('//div[@id="user-profile"]//div[contains(@class, "username")]/h2/span/text()', **self.xpathargs))
		if artist :
			artist = artist.strip()[1:]  # get rid of whitespace and '~' at the start
		else :
			raise BadOrMalformedResponse('could not find artist in html.')

		title = first(document.xpath('//div[@class="content"]//h2[@class="journal-title"]/text()', **self.xpathargs))
		if not title :
			self.logger.warning(f'could not find submission title in html. url: {self.url}')

		return {
			'url': self.formattedurl,
			'imageurl': None,
			'id': self.url,
			'resolution': None,
			'artist': artist,
			'artisturl': artisturl,
			'title': title,
			'rating': 'journal',
			'tags': ['journal'],
			'timestamp': upload.timestamp(),
			'thumbnails': None,
			'website': 'furaffinity',
		}
