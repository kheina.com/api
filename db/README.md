### For more information on how to get postgresql to work on Opensuse, please refer to [this guide](https://en.opensuse.org/SDB:PostgreSQL)  

These opensuse packages are required to run postgresql (with python):  
`postgresql`  
`postgresql-server`  
`postgresql-plpython`  
[optional: `postgresql-contrib`]  
  

Some handy terminal commands:  
`initdb -D db` will create a database server in directory `db`  
`pg_ctl -D db [-l logfile] start` will launch the server located in directory `db` (optionally using logfile `logfile`)  
`createdb kheina` will create a database called *kheina* in the currently running postgresql server  
