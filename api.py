from starlette.responses import UJSONResponse, PlainTextResponse
from common import HTTPError, databaser, parsesource
from kh_common.logging import getLogger
from kh_common.server import Request, Response, ServerApp
from common.safejoin import SafeJoin
from urllib.parse import urlparse
from traceback import format_tb
from common.cwd import SetCWD
import ujson as json
import time
import sys
import os


SetCWD()
staticDirectory = 'static'
logger = getLogger()
app = ServerApp(auth=False)


def handle_json(value) :
	try :
		with open(SafeJoin(value + '.json'), 'r') as jason :
			return json.load(jason)
	except FileNotFoundError :
		raise HTTPError.NotFound('The requested resource is not available.')


@app.post('/v1/search')
async def v1search(req: Request) :
	# key, user = authServer.verifyKey(req.session.get('login-key'), refreshKey=req.session.get('refresh-key'))
	# validate the request came from api.kheina.com

	log = { }
	formdata = await req.form()

	if 'file' in formdata :
		imagedata = formdata['file'].file.read()

	elif 'url' in formdata :
		log['imageurl'] = formdata['url']
		imagedata = parsesource.loadimagedata(formdata['url'])

	else : raise HTTPError.BadRequest('No form data provided.')

	if not imagedata :
		logdata = { k: v[:100] for k, v in formdata.items() }
		raise HTTPError.BadRequest(f'Form data was not able to be processed: {", ".join(list(logdata.keys()))}.', logdata=logdata)

	initialtime = time.time()
	results = databaser.queryiqdb(imagedata)
	elapsedtime = time.time() - initialtime
	log.update({
		'method': req.method,
		'url': str(req.url),
		'similarity': results[0]['similarity'],
		'elapsedtime': elapsedtime,
	})
	logger.info(log)
	return {
		'results': results,
		'elapsedtime': elapsedtime,
		'stats': handle_json('stats'),
		'error': None,
	}


@app.get('/stats')
async def v0stats() :
	return handle_json('stats')


databaser.start(logger, readonly=True)
logger.info('server started')


@app.on_event('shutdown')
async def shutdown() :
	databaser.cleanup()

if __name__ == '__main__' :
	from uvicorn.main import run
	run(app, host='127.0.0.1', port=80)
