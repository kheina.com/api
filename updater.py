from pycrawl.common import GetFullyQualifiedClassName
from common.MessageQueue import Receiver
from common import databaser, logging
from common.cwd import SetCWD
import ujson as json
import subprocess
import pika
import time
import sys
import os


# fetch working directory
SetCWD()
logger = logging.getLogger('updater', disable=['pika'])


baseUpdate = {
	'furaffinityItems': 0,
	'furrynetworkItems': 0,
	'newImages': 0,
	'newArtists': 0,
	'newSources': 0,
	'totalItems': 0,
	'crawledItems': 0,
}


class Updater(Receiver) :
	def __init__(self, looptime=900, TTL=None) :
		self.looptime = looptime
		if TTL is not None :
			self.TTL = time.time() + TTL
		else :
			self.TTL = float('inf')

		with open('credentials/updater.json') as credentials :
			credentials = json.load(credentials)
			self._connection_info = credentials['connection_info']
			self._exchange_info = credentials['exchange_info']
			self._channel_info = credentials['channel_info']
			self._route = credentials['routing_key']

	def run(self, **kwargs) :
		call = './iqdb/iqdb listen 7000 -r -s127.0.0.1 main.db'.split()
		# boot up iqdb, in case it wasn't already
		subprocess.Popen(call)

		while True :
			starttime = time.time()
			metadata = self.recv(kwargs.get('forcelist'))

			if metadata :  # feel free to skip if the queue was empty
				try :
					databaser.start(logger, readonly=False)
					dbr = databaser.updatedatabase(metadata)

				except OSError as e :
					logger.exception(baseUpdate)
					if 'Too many open files' not in str(e) :
						return

				except databaser.DatabaserError as e :
					logger.exception(baseUpdate)
					if 'could not connect to database kheina' in str(e) :
						return

				finally :
					databaser.cleanup()

				if dbr['newsources'] :
					subprocess.Popen(call)  # boot up iqdb as a background process

			else :
				# go ahead and log an empty update
				logger.info(baseUpdate)

			endtime = time.time()
			elapsedtime = endtime - starttime
			sleepfor = self.looptime - elapsedtime
			if sleepfor + endtime > self.TTL :
				return
			time.sleep(sleepfor)

	def recv(self, forcelist=False) :
		# just let it fail if it's not json serialized
		if forcelist :
			return list(map(json.loads, self._recv()))
		else :
			return map(json.loads, self._recv())


if __name__ == '__main__' :
	from ast import literal_eval
	kwargs = { k: literal_eval(v) for k, v in (arg.split('=') for arg in sys.argv[1:]) }
	try :
		updater = Updater()
		updater.run(**kwargs)
	finally :
		pass
